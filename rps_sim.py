import random

class RPS_Sim:

    def __init__(self, player1_name, player2_name, total_spots):        
        self.max_spot = (total_spots // 2) + 1#integer division
        self.player1_name = player1_name
        self.player2_name = player2_name
        self.init_variables()

    def init_variables(self):
        self.current_spot = 0
        self.round_count = 0
        self.winner = ""

    def simulate_game(self):
        self.play_round()
        if abs(self.current_spot) == self.max_spot:
            # print("-- GAME OVER --")
            # self.print_stats(self.winner)
            pass
        else:            
            self.simulate_game()

    def play_round(self):
        player1_choice = self.random_choice()
        player2_choice = self.random_choice()
        winner = self.determine_winner(player1_choice, player2_choice)
        self.winner = winner
        self.move_to_proper_spot(winner)
        # print("========== ROUND " + str(self.round_count) + "==========")
        # print("Outcome: " + winner)
        # print("Current Spot: " + str(self.current_spot))
        self.round_count += 1

    def move_to_proper_spot(self, winner):
        if winner == self.player1_name:
            self.current_spot -= 1
        elif winner == self.player2_name:
            self.current_spot += 1            

    # returns a winner's name OR blank (for tie)
    def determine_winner(self, player1_choice, player2_choice):
        winner = ""
        if player1_choice == "rock":
            if player2_choice == "scissors":
                winner = self.player1_name
            elif player2_choice == "paper":
                winner = self.player2_name
        elif player1_choice == "paper":
            if player2_choice == "scissors":
                winner = self.player2_name
            elif player2_choice == "rock":
                winner = self.player1_name
        elif player1_choice == "scissors":
            if player2_choice == "rock":
                winner = self.player2_name
            elif player2_choice == "paper":
                winner = self.player1_name
        return winner

    def random_choice(self):
        myRnd = random.randint(0,2)
        result = ""
        if myRnd == 0:
            result = "rock"
        elif myRnd == 1:
            result = "paper"
        elif myRnd == 2:
            result = "scissors"
        else:
            print("-- ERROR: there should NOT be a 4th choice")

        return result
        
    def print_stats(self, winner):
        # print("** Winner: " + winner)
        # print("Total Number of Rounds: " + str(self.round_count))
        pass