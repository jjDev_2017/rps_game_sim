from rps_sim import RPS_Sim
import statistics
import time 

class Stats:

    def __init__(self, simulation_count):
        self.total_sim_count = simulation_count
        self.rps_sim = RPS_Sim("player1", "player2", 5)
        self.round_count_records = []            

    def gather_stats(self):
        self.start_time = time.time()
        for i in range(0, self.total_sim_count):
            self.rps_sim.simulate_game()            
            self.round_count_records.append(self.rps_sim.round_count)
            self.rps_sim.init_variables()
        self.print_stats_results()
        
    def print_elapsed_time(self, start_time, end_time):
        diff_time = end_time - start_time
        diff_sec = diff_time % 60
        diff_minutes = diff_time // 60
        print("Time Elapsed: " + str(diff_time) + " seconds")
        

    def print_stats_results(self):
        self.end_time = time.time()
        self.print_elapsed_time(self.start_time, self.end_time)
        print("Median: " + str(statistics.median(self.round_count_records)) + " rounds")
        print("Mode: " + str(statistics.mode(self.round_count_records)) + " rounds")
        print("Max: " + str(max(self.round_count_records)) + " rounds")
        print("Min: " + str(min(self.round_count_records)) + " rounds")
        

stats = Stats(999999)
stats.gather_stats()
